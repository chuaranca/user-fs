'use strict'

import fs from 'fs';
import userData from '../../user.json';
import UserRequestModel from '../ui/request/User.request.model';
import MessageResponseModel from '../ui/response/Message.response.model';

var userArray = Array.from(userData);
const userController = {};

function writeFile(arrayData) {
    fs.writeFile('user.json', arrayData, (err) => {
        if (err) throw err;
        console.log('El archivo fue sobreescrito...');
    });
}

userController.login = (req, res) => {
    console.log('Inicio método login()');
    let {username, password} = req.body;
    let userLogin;
    userArray.forEach(element => {
        if (username == element.username && password == element.password) {
            res.status(200).send({status: 'SUCCESS', message: 'Usuario logeado correctamente.'});
            element.login = true;
            userLogin = element;
        }
    });

    if (userLogin == undefined) {   
        res.status(200).send({status: 'WARNING', message: 'Usuario o Contraseña incorrecta.'});
    } else {
        writeFile(JSON.stringify(userArray));
    }
    console.log('Fin método login()');
};

userController.logout = (req, res) => {
    console.log('Inicio método logout()');
    const id = req.body.id;
    
    userArray.forEach(element => {
        if (id == element.id) {
            if (element.login) {
                element.login = false;
                res.status(200).send({status: 'SUCCESS', message: 'Operación realizada correctamente.'});
                writeFile(JSON.stringify(userArray));
            } else {
                res.status(200).send({status: 'WARNING', message: 'El usuario no se encuentra en sesión.'});
            }
        }
    });
    console.log('Fin método logout()');
}

userController.getAllUsers = (req, res) => {
    console.log('Inicio método getAllUsers()');
    res.status(200).send(userData);
    console.log('Fin método getAllUsers()');
};

userController.getUser = (req, res) => {
    console.log('Inicio método getUser()');
    let returnValue;

    returnValue = userArray[req.params.id - 1];

    if (returnValue == undefined) {
        res.status(200).send({status: 'WARNING', message: 'Usuario NO encontrado.'});
    } else {
        res.status(200).send(returnValue);
    }
    console.log('Fin método getUser()');
};

userController.updateUser = (req, res) => {
    console.log('Inicio del método updateUser()');
    let userRequest = new UserRequestModel();
    Object.assign(userRequest, req.body);
    userRequest.id = req.params.id;
    let user = userArray[req.params.id - 1];

    if (user == undefined) {
        res.status(200).send({status: 'WARNING', message: 'Usuario NO encontrado.'});
    } else {
        Object.assign(user, userRequest);
        userArray.splice(req.params.id - 1, 1, user);
        res.status(200).send({status: 'SUCCESS', message: 'Usuario actualizado correctamente.'});
        writeFile(JSON.stringify(userArray));
    }

    console.log('Fin del método updateUser()');
};

userController.saveUser = (req, res) => {
    console.log('Inicio del método saveUser()');
    let userRequest = new UserRequestModel();
    Object.assign(userRequest, req.body);
    userRequest.id  = userArray.length + 1;
    let messageResponse = new MessageResponseModel();
    
    try {
        userArray.forEach(element => {
            if (element.username == userRequest.username) {
                messageResponse.status = 'WARNING';
                messageResponse.message = 'El username ingresado ya existe en nuestro sistema.';
                throw messageResponse;
            }
            if (element.email == userRequest.email) {
                messageResponse.status = 'WARNING';
                messageResponse.message = 'El email ingresado ya existe en nuestro sistema.';
                throw messageResponse;
            }
        });    
        userArray.push(userRequest);
        res.status(200).send({status: 'SUCCESS', message: 'Usuario registrado correctamente.'});
        writeFile(JSON.stringify(userArray));
    } catch (error) {
        if (error.status == 'WARNING') {
            res.status(200).send(error);
        } else {
            res.status(500).send({status: 'ERROR', message: 'Servicio no disponible, intente nuevamente en unos instantes.'});
        }
    }
    
    console.log('Fin del método saveUser()');
};


export default userController;
'use strict'

import express from 'express';
import bodyParser from 'body-parser';
import userRoute from './routes/user.routes';

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api-peru', userRoute);

export default app;
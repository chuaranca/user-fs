export default class MessageResponseModel {
    constructor(status, message) {
        this.status = status;
        this.message = message;
    }
}
'use strict'

import express from 'express'
import userController from '../controllers/user.controller';

let api = express.Router();

api.get('/login', userController.login);
api.get('/users', userController.getAllUsers);
api.get('/users/:id', userController.getUser);
api.post('/users', userController.saveUser);
api.put('/users/:id', userController.updateUser);
api.put('/logout', userController.logout);

export default api;